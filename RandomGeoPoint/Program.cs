﻿using System;
using RandomGeoPoint.DAL;
using RandomGeoPoint.Classes;

namespace RandomGeoPoint
{
    class Program
    {
        private static string tableName = Properties.Settings.Default.TableName; //Name of the table to be used
        private static int amountOfRecords = Properties.Settings.Default.AmountOfRecords; //The number of random recors to create
        private const int bufferHeight = 10020; //Amount of lines the console will display

        static void Main(string[] args)
        {
            Console.BufferHeight = bufferHeight;

            try
            {
                //Make sure there is a connection and the connection is open
                Console.WriteLine(OraDb.checkConn());
                Console.WriteLine("");

                //For test purposes, uncomment to remove the table if it exist
                //if (OraDb.checkTableExist(tableName))
                //{
                //    OraDb.dropTable(tableName);
                //}

                //See if table exists
                if (!OraDb.checkTableExist(tableName))
                {
                    Console.WriteLine("The table does not exist.");
                    Console.WriteLine("");

                    //Create table
                    bool tableCreated = OraDb.createTable(tableName);

                    if (tableCreated)
                    {
                        //Insert random data
                        Helper.InsertRandomRecords(amountOfRecords);

                        Console.WriteLine("Table created and data added.");
                        Console.WriteLine("");
                    }

                    //Check if the data is valid
                    Console.WriteLine("Check if the data is valid:");
                    Console.WriteLine(OraDb.checkDataValid(tableName));
                    Console.WriteLine("");

                    //Sort and print data to screen
                    Helper.PrintToScreen();
                }
                else
                {
                    Console.WriteLine("The table exist.");
                    Console.WriteLine("");

                    //Check if the data is valid
                    Console.WriteLine("Check if the data is valid:");
                    Console.WriteLine(OraDb.checkDataValid(tableName));
                    Console.WriteLine("");

                    //Sort and print data to screen   
                    Helper.PrintToScreen();
                }


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
