﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using RandomGeoPoint.DAL;

namespace RandomGeoPoint.Classes
{
    class Helper
    {
        private static readonly Random random = new Random();

        public static double RandomNumberBetween(double minValue, double maxValue)
        {
            var next = random.NextDouble();
            return minValue + (next * (maxValue - minValue));
        }

        public static void PrintToScreen()
        {
            GeoAngle refLat = GeoAngle.FromDouble(GeoPointDetail._refLat);
            GeoAngle refLon = GeoAngle.FromDouble(GeoPointDetail._refLon);

            Console.WriteLine(
                "The reference latitude and longitude is the Blackbox Control office." +
                Environment.NewLine +
                " Decimal: " + GeoPointDetail._refLat.ToString() + ", " + GeoPointDetail._refLon.ToString() +
                Environment.NewLine +
                " Degrees: " + refLat.ToString("Lat") + ", " + refLon.ToString("Lon") +
                Environment.NewLine
                );

            DataView dv = new DataView();
            dv = OraDb.getAllRecords(Properties.Settings.Default.TableName);

            List<GeoPointDetail> LatLonList = new List<GeoPointDetail>();

            foreach (DataRow row in dv.Table.Rows)
            {
                string name = row["name"].ToString();
                double lat = Convert.ToDouble((decimal)row["latitude"]);
                double lon = Convert.ToDouble((decimal)row["longitude"]);
                GeoPointDetail ll = new GeoPointDetail(lat, lon, name);

                LatLonList.Add(ll);
            }

            List<GeoPointDetail> SortedList = LatLonList.OrderBy(o => o.Distance).ToList();

            foreach (GeoPointDetail ll in SortedList)
            {
                GeoAngle lat = GeoAngle.FromDouble(ll.Latitude);
                GeoAngle lon = GeoAngle.FromDouble(ll.Longtitude);
                Console.WriteLine(
                    ll.Name + ": " +
                    Environment.NewLine +
                    " Decimal: " + ll.Latitude.ToString() +
                    ", " + ll.Longtitude.ToString() +
                    Environment.NewLine +
                    " Degrees: " + lat.ToString("Lat") +
                    ", " + lon.ToString("Lon") +
                    Environment.NewLine +
                    " Dist: " + ll.Distance.ToString() + "km" +
                    Environment.NewLine
                    );
            }
        }

        public static void InsertRandomRecords(int amount)
        {
            for (int i = 1; i <= amount; i++)
            {
                double lat = RandomNumberBetween(-90, 90);
                double lon = RandomNumberBetween(-180, 180);
                OraDb.addRecord
                    (i,
                    "Position " + i.ToString(),
                    lat,
                    lon);
            }
        }

        private static double ToRad(double input)
        {
            return input * (Math.PI / 180);
        }

        private const double EARTH_RADIUS_KM = 6371;

        public static double GetDistanceKM(GeoPoint point1, GeoPoint point2)
        {
            //Implementation of the Haversine formula
            double dLat = ToRad(point2.Latitude - point1.Latitude);
            double dLon = ToRad(point2.Longtitude - point1.Longtitude);

            double a = Math.Pow(Math.Sin(dLat / 2), 2) +
                       Math.Cos(ToRad(point1.Latitude)) * Math.Cos(ToRad(point2.Latitude)) *
                       Math.Pow(Math.Sin(dLon / 2), 2);

            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            double distance = EARTH_RADIUS_KM * c;
            return distance;
        }
    }
}
