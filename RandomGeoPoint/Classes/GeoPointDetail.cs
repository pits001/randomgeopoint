﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomGeoPoint.Classes
{
    class GeoPointDetail : GeoPoint
    {
        private double _distance;

        public static double _refLat = Properties.Settings.Default.refLat;
        public static double _refLon = Properties.Settings.Default.refLon;

        public string Name { get; set; }

        public double Distance //Read only property
        {
            get { return _distance; }
        }

        private GeoPoint refGeoPoint //Read only private property
        {
            get { return new GeoPoint(_refLat, _refLon); }
        }

        public GeoPointDetail (double latitude, double longitude, string name) : base(latitude, longitude)
        {
            Name = name;
            _distance = Math.Round(Helper.GetDistanceKM(refGeoPoint, new GeoPoint(latitude, longitude)), 3);
        }
    }
}
