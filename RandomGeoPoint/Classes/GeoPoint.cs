﻿namespace RandomGeoPoint.Classes
{
    class GeoPoint
    {
        public double Latitude { get; set; }
        public double Longtitude { get; set; }

        public GeoPoint(double latitude, double longtitude)
        {
            Latitude = latitude;
            Longtitude = longtitude;
        }
    }
}
