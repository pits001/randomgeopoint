﻿using System;
using Oracle.ManagedDataAccess.Client;
using System.Data;


namespace RandomGeoPoint.DAL
{
    class OraDb
    {
        public static OracleConnection conn()
        {
            string oraDB = Properties.Settings.Default.ConnString;
            OracleConnection oraconn = new OracleConnection();
            try
            {
                oraconn.ConnectionString = oraDB;
                oraconn.Open();
                return oraconn;
            }
            catch (OracleException ex)
            {
                Console.WriteLine(ex.Message);
                return oraconn;
            }
        }

        public static bool checkTableExist(string tableName)
        {
            bool tableExist = false;

            try
            {
                using (OracleConnection conn = OraDb.conn())
                {
                    using (OracleCommand cmd = new OracleCommand(conn.ConnectionString, conn))
                    {
                        cmd.Parameters.Add("table", tableName);
                        cmd.CommandText = "select count(*) from user_tables where table_name = UPPER(:1)";
                        cmd.CommandType = CommandType.Text;
                        //cmd.ExecuteNonQuery();
                        OracleDataReader dr = cmd.ExecuteReader();
                        dr.Read();
                        if (dr.GetDecimal(0) != 0)
                        {
                            tableExist = true;
                        }
                    }
                }
            }
            catch (OracleException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return tableExist;
        }

        public static bool createTable(string tableName)
        {
            bool success = false;

            try
            {
                using (OracleConnection conn = OraDb.conn())
                {
                    using (OracleCommand cmd = new OracleCommand(conn.ConnectionString, conn))
                    {
                        cmd.CommandText = "CREATE TABLE " + tableName + " (id NUMBER(10) NOT NULL, name VARCHAR2(150), latitude FLOAT, longitude FLOAT)";
                        cmd.CommandType = CommandType.Text;
                        int result = cmd.ExecuteNonQuery();

                        if (result == -1)
                        {
                            success = true;
                        }
                    }
                }
            }
            catch (OracleException ex)
            {
                Console.WriteLine(ex.Message);
            }


            return success;
        }

        public static void addRecord(int id, string name, double lat, double lon)
        {
            try
            {
                using (OracleConnection conn = OraDb.conn())
                {
                    using (OracleCommand cmd = new OracleCommand(conn.ConnectionString, conn))
                    {

                        OracleParameter[] prm = new OracleParameter[4];
                        prm[1] = cmd.Parameters.Add("prmID", OracleDbType.Double, id, ParameterDirection.Input);
                        prm[1] = cmd.Parameters.Add("prmName", OracleDbType.Varchar2, name, ParameterDirection.Input);
                        prm[2] = cmd.Parameters.Add("prmLat", OracleDbType.Double, lat, ParameterDirection.Input);
                        prm[3] = cmd.Parameters.Add("prmLat", OracleDbType.Double, lon, ParameterDirection.Input);

                        cmd.CommandText = "insert into table1 (id, name, latitude, longitude) values (:1, :2, :3, :4)";
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (OracleException ex)
            {
                Console.WriteLine(ex.Message);
            }


        }

        public static DataView getAllRecords(string tableName)
        {
            DataTable dt = new DataTable();

            try
            {
                using (OracleConnection conn = OraDb.conn())
                {
                    using (OracleCommand cmd = new OracleCommand(conn.ConnectionString, conn))
                    {
                        cmd.CommandText = "select name, latitude, longitude from " + tableName;
                        cmd.CommandType = CommandType.Text;
                        OracleDataAdapter da = new OracleDataAdapter(cmd);
                        da.Fill(dt);
                    }
                }
            }
            catch (OracleException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return dt.DefaultView;
        }

        public static void dropTable(string tableName)
        {
            try
            {
                using (OracleConnection conn = OraDb.conn())
                {
                    using (OracleCommand cmd = new OracleCommand(conn.ConnectionString, conn))
                    {
                        cmd.CommandText = "DROP TABLE " + tableName + " PURGE";
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (OracleException ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        public static string checkConn()
        {
            try
            {
                using (OracleConnection conn = OraDb.conn())
                {
                    return "Connected to Oracle " + conn.ServerVersion;
                }
            }
            catch (OracleException ex)
            {
                Console.WriteLine(ex.Message);
                return ex.Message;
            }
        }

        public static string checkDataValid(string tableName)
        {
            try
            {
                using (OracleConnection conn = OraDb.conn())
                {
                    using (OracleCommand cmd = new OracleCommand(conn.ConnectionString, conn))
                    {
                        cmd.CommandText = "select MIN(id), MIN(latitude), MIN(longitude), MAX(id), MAX(latitude), MAX(longitude) from " + tableName;
                        cmd.CommandType = CommandType.Text;
                        OracleDataReader dr = cmd.ExecuteReader();
                        dr.Read();

                        return
                            "ID Min: " + dr.GetDecimal(0).ToString() +
                            ",ID Max: " + dr.GetDecimal(3).ToString() +
                            Environment.NewLine +
                            "Latitude Min: " + dr.GetDecimal(1).ToString() +
                            ", Latitude Max: " + dr.GetDecimal(4).ToString() +
                            Environment.NewLine +
                            "Longitude Min: " + dr.GetDecimal(2).ToString() +
                            ", Longitude Max: " + dr.GetDecimal(5).ToString();
                    }
                }
            }
            catch (OracleException ex)
            {
                Console.WriteLine(ex.Message);
                return ex.Message;
            }
        }
    }
}
